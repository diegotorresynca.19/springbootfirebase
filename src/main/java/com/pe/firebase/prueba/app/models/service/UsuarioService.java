package com.pe.firebase.prueba.app.models.service;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.pe.firebase.prueba.app.models.domain.Usuarios;

public interface UsuarioService {
	
	public String saveUsuario(Usuarios usuarios) throws InterruptedException, ExecutionException;
	
	public Usuarios findById(String cod) throws InterruptedException, ExecutionException;
	
	public List<Usuarios> findAllUsuarios() throws InterruptedException, ExecutionException;
	
	public String deleteUsuarios(String cod) throws InterruptedException, ExecutionException;
	
	public String updateUsuarios(Usuarios usuarios) throws InterruptedException, ExecutionException;

}
