package com.pe.firebase.prueba.app.models.domain;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Usuarios {
	
	private String cod;
	private String nombres;
	private String apellidos;
	private String edad;
	private Date fechanac;

}
