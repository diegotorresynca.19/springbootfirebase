package com.pe.firebase.prueba.app.controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pe.firebase.prueba.app.models.domain.Usuarios;
import com.pe.firebase.prueba.app.models.service.UsuarioService;

@RestController
public class UsuariosController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping("/usuarios")
	public String createUsarios(@RequestBody Usuarios usuarios) throws InterruptedException, ExecutionException{
		return usuarioService.saveUsuario(usuarios);
	}
	
	@GetMapping("/getUsuarios/{cod}")
	public Usuarios getUsuarios(@PathVariable String cod) throws InterruptedException, ExecutionException {
		return usuarioService.findById(cod);
	}
	
	@GetMapping("/getAllUsuarios")
	public List<Usuarios> getAllUsuario() throws InterruptedException, ExecutionException {
		return usuarioService.findAllUsuarios();
	}
	
	@PutMapping("/updateUsuarios") 
	public String updateUsusarios(@RequestBody Usuarios usuarios) throws InterruptedException, ExecutionException {
		return usuarioService.updateUsuarios(usuarios);
	}
	
	@DeleteMapping("/deleteUsuario/{cod}")
	public String deleteUser(@PathVariable String cod) throws InterruptedException, ExecutionException {
		return usuarioService.deleteUsuarios(cod);
	}
}
