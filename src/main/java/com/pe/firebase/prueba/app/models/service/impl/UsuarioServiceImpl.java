package com.pe.firebase.prueba.app.models.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import com.pe.firebase.prueba.app.models.domain.Usuarios;
import com.pe.firebase.prueba.app.models.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Override
	public String saveUsuario(Usuarios usuarios) throws InterruptedException, ExecutionException {
		Firestore db = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> future = db.collection("usuarios").document(usuarios.getCod()).set(usuarios);
        return future.get().getUpdateTime().toString();
	}

	@Override
	public Usuarios findById(String cod) throws InterruptedException, ExecutionException {
		Firestore db = FirestoreClient.getFirestore();
		DocumentReference docRef = db.collection("usuarios").document(cod);
		// asynchronously retrieve the document
		ApiFuture<DocumentSnapshot> future = docRef.get();
		// block on response
		DocumentSnapshot document = future.get();
		Usuarios usuarios = null;
		if (document.exists()) {
		  // convert document to POJO
			usuarios = document.toObject(Usuarios.class);
		  System.out.println(usuarios);
		  return usuarios;
		} else {
		  System.out.println("No such document!");
		  return null;
		}
	}

	@Override
	public String deleteUsuarios(String cod) throws InterruptedException, ExecutionException {
		Firestore db = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> writeResult = db.collection("usuarios").document(cod).delete();
		return writeResult.get().getUpdateTime().toString();
	}

	@Override
	public String updateUsuarios(Usuarios usuarios) throws InterruptedException, ExecutionException {
		Firestore db = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> future = db.collection("usuarios").document(usuarios.getCod()).set(usuarios);
		return future.get().getUpdateTime().toString();
	}



	@Override
	public List<Usuarios> findAllUsuarios() throws InterruptedException, ExecutionException {
		Firestore db = FirestoreClient.getFirestore();
		ApiFuture<QuerySnapshot> docRef = db.collection("usuarios").get();
		
		List<QueryDocumentSnapshot> documents = docRef.get().getDocuments();
		ArrayList<Usuarios> lstUsuario = new ArrayList<>();
		Usuarios usuarios = new Usuarios();
		for (QueryDocumentSnapshot document : documents) {
			usuarios = document.toObject(Usuarios.class);
			//System.out.println(document.getId() + " => " + document.toObject(Usuarios.class));
			lstUsuario.add(usuarios);
		}
		return lstUsuario;
	}

	

}
